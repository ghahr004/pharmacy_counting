
import sys

# We define the following class for the challenge where
# it gets a list of string records of the form 
# 'id,prescriber_last_name,prescriber_first_name,drug_name,drug_cost'
# and gives out the result that the challene is expecting.
class insightchallenge:
    def __init__(self, list_of_recs):
        self.list_of_recs = list_of_recs 
# The following function does a similar procedure 
# as the famous word count problem.
    def top_cost_drug(self):
        dict_of_drug_pres_cost = {}
        final_dict={}
        for a in self.list_of_recs:
# Since every line in the list_of_recs is of the form 
# 'id,prescriber_last_name,prescriber_first_name,drug_name,drug_cost'
# using the split(',') function we split each line of it into 
# a list of the form
# ['id','prescriber_last_name','prescriber_first_name','drug_name','drug_cost']
# In the dictionary dict_of_drug_pres_cost we set the keys equal to the drug_name
# which stands in the 3rd place of the above list and the values are set to be
# of lists consisted of elements of the form 
# (('prescriber_last_name','prescriber_first_name'), drug_cost) in which
# 'prescriber_last_name' stands in the 1st place (assuming that the lists
# are enumerated from 0), 'prescriber_first_name' stands in the 2nd place
# and 'drug_cost' stands in the 4th place.
            b = a.split(',')
            if b[3] in dict_of_drug_pres_cost.keys():
                dict_of_drug_pres_cost[b[3]].append(((b[1],b[2]), b[4]))
            else:
                dict_of_drug_pres_cost[b[3]] = [((b[1],b[2]), b[4])]
# Done with the set of the dictionary as described above, we need to 
# collect the number of prescribers and the costs involved
# with each key in the dictionary dict_of_drug_pres_cost. 
# Therefore we have the following look where 
# list_of_drs indicate the list of prescribers
# and list of costs indicate the list of costs for 
# a given key which is the drug name.
            for key in dict_of_drug_pres_cost.keys():
                value = dict_of_drug_pres_cost[key]
                list_of_drs = []
                list_of_costs = []
                for tuples in value:
                    list_of_drs.append(tuples[0])
                    list_of_costs.append(int(float(tuples[1])))
# Since we want the number of unique prescribers, we take
# the length of the set of list_of_drs and since we want
# the sum of all costs for a given drug, we calculate the
# sum of the list, list_of_costs.
                final_dict[key] = (len(set(list_of_drs)), sum(list_of_costs))
# Now it is time to prepare our output.
# Using the final_dict where each element is of the form 
# drug_name:(unique number of prescribers, total cost of the drug) we 
# convert it to the form (drug_name, unique number of prescribers, total cost of the drug) 
# as follows:
        final_result_in_tuples = map(lambda rec: (rec[0],rec[1][0], rec[1][1]), final_dict.items())
# We sort our data in descending way as follows:    
        final_result_in_tuples_ordered_asc = sorted(final_result_in_tuples, key=lambda rec: (rec[2], rec[0]))
# We reverse the previous list so the data will be sorted ascendingly by 
# the total drug cost and if there is a tie, drug name.
        final_result_in_tuples_ordered_desc = final_result_in_tuples_ordered_asc[::-1]
# We prepare the lines:    
        final_result_lines = map(lambda rec: str(rec[0]) + ',' + str(rec[1]) + ',' + str(rec[2]), final_result_in_tuples_ordered_desc)
# We add the line  
# 'drug_name,num_prescriber,total_cost'
# so the final_result_lines will be in the format requested by the challenge.
        final_result_lines.insert(0,'drug_name,num_prescriber,total_cost')
        return final_result_lines


data = open(str(sys.argv[1]), "r")
data_read = data.read()
data_split_in_lines = data_read.splitlines()
data_split_in_lines.pop(0)


output = open(str(sys.argv[2]),'w')

for a in insightchallenge(data_split_in_lines).top_cost_drug():
    output.write(a + "\n")
output.close()

