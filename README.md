# Summary of the challenge
I got the pharamcy_counting coding challenge from the insight data engineering fellows program on 10th of July
2018 and it was due a week later. The data 
was a subset of the data located in https://drive.google.com/file/d/1fxtTLR_Z5fTO-Y91BnKOQd6J0VC9gPO3/view
where 24 millions of records exist.
The original dataset was obtained from the Centers for Medicare & Medicaid Services but has been cleaned and 
simplified to match the scope of the coding challenge.  The data is of the following format:


```
id,prescriber_last_name,prescriber_first_name,drug_name,drug_cost
1000000001,Smith,James,AMBIEN,100
1000000002,Garcia,Maria,AMBIEN,200
1000000003,Johnson,James,CHLORPROMAZINE,1000
1000000004,Rodriguez,Maria,CHLORPROMAZINE,2000
1000000005,Smith,David,BENZTROPINE MESYLATE,1500
```
in which the first, second, third, forth and the fifth columns are 
id of the prescriber, prescriber's last name, prescriber's first name, drug name and the drug cost. 
The challenge asked me  to generate a list of all drugs, the total number of UNIQUE individuals
who prescribed the medication, and the total drug cost, which must be listed in descending order based on the total
drug cost and if there is a tie, drug name and create the output file, `top_cost_drug.txt`, that contains comma (`,`)
separated fields in each line in which each line of this file contains these fields:
* drug_name: the exact drug name as shown in the input dataset
* num_prescriber: the number of unique prescribers who prescribed the drug. For the purposes of this challenge, a prescriber is considered the same person if two lines share the same prescriber first and last names
* total_cost: total cost of the drug across all prescribers


then my output file, **`top_cost_drug.txt`**, would contain the following lines
```
drug_name,num_prescriber,total_cost
CHLORPROMAZINE,2,3000
BENZTROPINE MESYLATE,1,1500
AMBIEN,2,300
```

# Summary of my approach

We define a class named ```insightchallenge ``` for the challenge where
it gets a list of string records of the form 
```id,prescriber_last_name,prescriber_first_name,drug_name,drug_cost```
and gives out the result that the challenge is expecting. 

Inside the class we define a method named ```top_cost_drug ``` where it first
for each line of records (except the first line which describes the columns)
gets the 3rd element of the line which is drug_name as the key and puts
the value as ```[(prescriber_last_name, prescriber_first_name), drug_cost]```. Similar
to the famous word count problem, if the ```drug_name``` is repeated, the values will be 
appended.

Since every line in the ```list_of_recs```  is of the form 
```'id,prescriber_last_name,prescriber_first_name,drug_name,drug_cost'```
using the ```split(',')``` function we split each line of it into 
a list of the form
```['id','prescriber_last_name','prescriber_first_name','drug_name','drug_cost']```
In the dictionary dict_of_drug_pres_cost we set the keys equal to the drug_name
which stands in the 3rd place of the above list and the values are set to be
of lists consisted of elements of the form 
```(('prescriber_last_name','prescriber_first_name'), drug_cost)``` in which
'prescriber_last_name' stands in the 1st place (assuming that the lists
are enumerated from 0), 'prescriber_first_name' stands in the 2nd place
and 'drug_cost' stands in the 4th place.


Once we are done with the above dictionary we define another dictionary called ```final_dict``` in which
we collect the number of prescribers and the costs involved
with each key in the dictionary ```dict_of_drug_pres_cost```. In the other words, the keys in ```final_dict```
will be the same as in ```dict_of_drug_pres_cost``` while the values are of type 
```(number of unique prescribers, some of the costs)```.
Therefore we have the lists where 
list_of_drs indicate the list of prescribers
and list of costs indicate the list of costs for 
a given key which is the drug name.

Since we want the number of unique prescribers, we take
the length of the set of ```list_of_drs``` and since we want
the sum of all costs for a given drug, we calculate the
sum of the list, ```list_of_costs```.

In preparation for the output, using the ```final_dict``` where each element is of the form 
```drug_name:(unique number of prescribers, total cost of the drug)``` we 
convert it to the form ```(drug_name, unique number of prescribers, total cost of the drug)```
and then sort it using Python's built in function ```sorted```. To be more precise, we put all the records
of ```final_dict``` in ```final_result_in_tuples``` were the records are of the form
```(drug_name, unique number of prescribers, total cost of the drug)``` then using the built in function
```sorted``` we sort first by the third component of ```final_result_in_tuples``` which is the
```total_cost``` and then by the first component which is the ```drug_name``` and call the
resulted list as ```final_result_in_tuples_ordered_asc``` but since such
a sorting will be ascending, we make it descending by defining 
```final_result_in_tuples_ordered_desc``` to be ```final_result_in_tuples_ordered_asc[::-1]```.

I used 4 tests to see if the output fits with the requirements of the challenge and 
thanks to insight_testsuit folder, my output got success for 4 different outputs.



The directory structure for this repo should look like this:

    ├── README.md 
    ├── run.sh
    ├── src
    │   └── pharmacy-counting.py
    ├── input
    │   └── itcont.txt
    ├── output
    |   └── top_cost_drug.txt
    ├── insight_testsuite
        └── run_tests.sh
        └── tests
            └── test_1
            |   ├── input
            |   │   └── itcont.txt
            |   |__ output
            |   │   └── top_cost_drug.txt
            ├── test_2
            |    ├── input
            |    │   └── itcont.txt
            |    |── output
            |        └── top_cost_drug.txt
            |___ test_3       
            |   ├── input
            |   │   └── itcont.txt
            |   |__ output
            |   │   └── top_cost_drug.txt
            ├── test_4
                ├── input
                │   └── itcont.txt
                |── output
                    └── top_cost_drug.txt

